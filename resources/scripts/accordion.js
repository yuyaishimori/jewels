$(function(){
	$('.JS_Click_SlideAccordion_Trigger').on('click', function(){

		$(this).next().slideToggle();
		$('.isActive').removeClass('isActive');
		$(this).addClass('isActive');

		$('.JS_Click_SlideAccordion_Trigger').not($(this)).next().slideUp();

	});

	$('.JS_Click_ShowSubContent_Trigger').on('click', function(){

		$(this).children('.Area-Inner-PF-Box-Item-Sub').toggleClass('isShow');

		$('.JS_Click_ShowSubContent_Trigger').not($(this)).children('.Area-Inner-PF-Box-Item-Sub').removeClass('isShow');

	});

	$('.JS_Click_ShowSearchMenu_Trigger').on('click', function(){
		if($(this).attr('src').indexOf('show') != -1){
			$(this).attr('src', './resources/images/storelist-searchclose-icon.png');
		} else{
			$(this).attr('src', './resources/images/storelist-searchshow-icon.png');
		}
    $('.Area-Inner-Content').slideToggle();
	});

	// サブメニューの要素をクリックした時に親要素のイベントが発火しないようにする
  $(".JS_SubItem_List").on("click", function(event){
    event.stopPropagation();
  });

});
