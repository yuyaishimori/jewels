$(function(){

  var swiper = new Swiper('.swiper-container', {
    loop: true,
    slidesPerView: 5,
    initialSlide: 1,
    breakpoints: {
      540: {
        slidesPerView: 2,
        initialSlide: 2,
      }
    },
    autoplay: {
      delay: 2000,
    },
  });

});
