$(function(){

  $('.JS_Click_ShowMenu_Trigger').on('click', function(){
    $('.MenuLayer').addClass('isShow');
    $('.MenuLayer').removeClass('isHidden');
  });

  $('.JS_Click_CloseMenu_Trigger').on('click', function(){
    $('.MenuLayer').removeClass('isShow');
    $('.MenuLayer').addClass('isHidden');
  });

  $('.Menu-Inner-List-Item').each(function(){
    $(this).on('click', function(){
      $(this).find('.Menu-Inner-List-Item-Link').click();
      $('.MenuLayer').removeClass('isShow');
      $('.MenuLayer').addClass('isHidden');
    })
  });

  $('.Menu-Inner-List-Item-Link').on('click', function(){
    $('.MenuLayer').removeClass('isShow');
    $('.MenuLayer').addClass('isHidden');
  })

  $(window).on("scroll", function() {
    scrollHeight = $(document).height();
    scrollPosition = $(window).height() + $(window).scrollTop();
    footerHeight = $(".FooterSection").innerHeight(); //footerの高さ（＝止めたい位置）
    if ( scrollHeight - scrollPosition  <= footerHeight ) {
      $(".CTASection").css({
        "position":"absolute",
        "bottom": "54px",
        "border-top": "none",
        "padding-top": "0px",
        "padding-bottom": "0px"
      });
    } else {
      $(".CTASection").css({
        "position":"fixed",
        "bottom": "0px",
        "border-top": "1px solid #eee",
        "padding-top": "14px",
        "padding-bottom": "14px"
      });
    }
  });

  //ページ内リンクスムーススクロール
  $('a[href^="#"]').on('click', function () {
      var href = $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top-80;
      $("html, body").animate({scrollTop: position}, 550, "swing");
      return false;
  });

  function ScrollTop() {
    $('html, body').animate({
        scrollTop: 0
    }, 500);
  }

  //トップにもどるボタン
  $('.JS_Click_ScrollTop_Trigger').click(function () {
      ScrollTop()
  });

});
